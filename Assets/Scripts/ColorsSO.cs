﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ColorsSO",menuName ="ScriptableObjects/Colors",order = 1)]
public class ColorsSO : ScriptableObject {

    [TextArea(4,10)]
    public string legend;
    public Color[] colorPalette;

    public Color GetColor(int index)
    {
        return colorPalette[index];
    }

    public void SetMaterialColor(GameObject target, int index)
    {
        if (target.GetComponent<Material>() != null)
            target.GetComponent<Material>().color = GetColor(index);
        else
            Debug.LogWarning("No Material component in game object! Color won't be set.");
    }
}
