﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SummaryController : MonoBehaviour {

    public Image bgBlend;
    public Image filler;
    public TMPro.TextMeshProUGUI RoundTMP;
    public Player player;
    public Player opponent;
    public int roundNumber;
    public GameObject nextRound;
    public PlayerData playerData;
    public PlayerData opponentData;

    public float fillTimeStep;
    public AnimationCurve popCurve;
    float curveTime;
    public static SummaryController instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        SetRound();
        filler.fillAmount = 0f;
        player.SetPlayer(playerData.playerType, 
            playerData.playerName, 
            playerData.fishType, 
            playerData.roundPoints, 
            playerData.points, 
            playerData.sizePoints, 
            playerData.accuracyPoints, 
            playerData.timePoints, 
            playerData.targetPoints, 
            playerData.isWinner);
        opponent.SetPlayer(opponentData.playerType,
            opponentData.playerName,
            opponentData.fishType,
            opponentData.roundPoints,
            opponentData.points,
            opponentData.sizePoints,
            opponentData.accuracyPoints,
            opponentData.timePoints,
            opponentData.targetPoints,
            opponentData.isWinner);

        if (player.isWinner && !opponent.isWinner)
            StartCoroutine(FillWinner(PlayerType.Player));
        else if (opponent.isWinner && !player.isWinner)
            StartCoroutine(FillWinner(PlayerType.Opponent));

        StartCoroutine(NextRoundPop());
    }

    IEnumerator FillWinner(PlayerType playerType)
    {
        float _fillAmount = 0f;
        switch (playerType)
        {
            case PlayerType.Player:
                filler.fillOrigin = 0;
                break;
            case PlayerType.Opponent:
                filler.fillOrigin = 1;
                break;
        }

        while(_fillAmount != 0.5f)
        {
            _fillAmount += 0.01f;
            filler.fillAmount = _fillAmount;
            if (filler.fillAmount > 0.5f)
            {
                filler.fillAmount = 0.5f;
                yield break;
            }
            yield return new WaitForSeconds(fillTimeStep);
        }
 
    }

    void SetRound()
    {
        RoundTMP.text = "ROUND " + roundNumber.ToString() + " RESULTS";
    }

    public IEnumerator NextRoundPop()
    {
        curveTime = 0;
        while (true)
        {
            curveTime += Time.deltaTime;
            nextRound.transform.localScale = new Vector3(popCurve.Evaluate(curveTime), popCurve.Evaluate(curveTime), popCurve.Evaluate(curveTime));
            yield return null;
        }
    }
}

[Serializable]
public class PlayerData
{
    public string playerName;
    public int roundPoints;
    public int points;
    public int sizePoints;
    public int accuracyPoints;
    public int timePoints;
    public int targetPoints;
    public PlayerType playerType;
    public FishType fishType;
    public bool isWinner;
}

public enum PlayerType
{
    Player,
    Opponent
}

public enum FishType
{
    Bluefin,
    Runner
}
