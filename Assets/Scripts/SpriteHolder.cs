﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="SpritesSO",menuName ="ScriptableObjects/SpriteHolder",order =2)]
public class SpriteHolder : ScriptableObject {

    [TextArea(4, 4)]
    public string legend;
    public Sprite[] sprites;

    public Sprite GetFishSprite(FishType fish)
    {
        switch(fish)
        {
            case FishType.Bluefin: return sprites[(int)FishType.Bluefin];
            case FishType.Runner: return sprites[(int)FishType.Runner];
            default: return sprites[0];
        }
    }

    public Sprite GetPlayerSprite(PlayerType player)
    {
        switch (player)
        {
            case PlayerType.Player: return sprites[(int)PlayerType.Player];
            case PlayerType.Opponent: return sprites[(int)PlayerType.Opponent];
            default: return sprites[0];
        }
    }
}
