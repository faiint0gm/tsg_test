﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorsHolder : MonoBehaviour {

    public ColorsSO textColors;

    public static ColorsHolder instance;

    private void Awake()
    {
        instance = this;
    }
}
