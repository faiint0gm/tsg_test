﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Player : MonoBehaviour {

    public Image portrait;
    public Image fishImage;
    public TextMeshProUGUI roundPointsTMP;
    public TextMeshProUGUI nameTMP;
    public GameObject roundWinner;
    public TextMeshProUGUI fishTMP;
    public GameObject pts;
    public TextMeshProUGUI pointsTMP;
    public TextMeshProUGUI sizePointsTMP;
    public TextMeshProUGUI accuracyPointsTMP;
    public TextMeshProUGUI timePointsTMP;
    public TextMeshProUGUI targetPointsTMP;
    public SpriteHolder fishSprites;
    public SpriteHolder playersSprites;

    public AnimationCurve textFromLeft;
    public AnimationCurve textFromRight;

    string playerName;
    string fishName;
    int roundPoints;
    int points;
    int sizePoints;
    int accuracyPoints;
    int timePoints;
    int targetPoints;

    [HideInInspector]
    public bool isWinner;

    void SetAllTexts()
    {
        roundPointsTMP.text = roundPoints.ToString();
        nameTMP.text = playerName;
        fishTMP.text = fishName;
        pointsTMP.text = points.ToString();
        sizePointsTMP.text = "+" + sizePoints.ToString();
        accuracyPointsTMP.text = "+" + accuracyPoints.ToString();
        timePointsTMP.text = "+" + timePoints.ToString();
        targetPointsTMP.text = "+" + targetPoints.ToString();
    }

    public void SetPlayer(PlayerType _playerType, string _playerName, FishType fishType, int _roundPoints, int _points, int _sizePoints, int _accuracyPoints, int _timePoints, int _targetPoints, bool _isWinner)
    {
        fishImage.preserveAspect = true;
        portrait.preserveAspect = true;
        nameTMP.color = ColorsHolder.instance.textColors.GetColor(_playerType == PlayerType.Player ? 2 : 3);
        roundPointsTMP.color = ColorsHolder.instance.textColors.GetColor(_playerType == PlayerType.Player ? 2 : 3);
        playerName = _playerName;
        portrait.sprite = playersSprites.GetPlayerSprite(_playerType);
        roundPoints = _roundPoints;
        points = _points;
        sizePoints = _sizePoints;
        accuracyPoints = _accuracyPoints;
        timePoints = _timePoints;
        targetPoints = _targetPoints;
        fishImage.sprite = fishSprites.GetFishSprite(fishType);
        isWinner = _isWinner;
        roundWinner.SetActive(isWinner);
        fishName = fishType.ToString();

        pts.SetActive(false);
        pointsTMP.gameObject.SetActive(false);
        sizePointsTMP.gameObject.SetActive(false);
        accuracyPointsTMP.gameObject.SetActive(false);
        timePointsTMP.gameObject.SetActive(false);
        targetPointsTMP.gameObject.SetActive(false);

        SetAllTexts();
        StartCoroutine(RunAnimationsForType(_playerType));
    }

    IEnumerator RunAnimationsForType(PlayerType playerType)
    {
        switch(playerType)
        {
            case PlayerType.Player:
               yield return StartCoroutine(RunAnimation(pointsTMP.gameObject, textFromLeft, -100, 0.5f));
                yield return StartCoroutine(RunAnimation(pts, textFromLeft, -100, 0.2f));
                yield return StartCoroutine(RunAnimation(sizePointsTMP.gameObject, textFromLeft, -100, 0.5f));
                yield return StartCoroutine(RunAnimation(accuracyPointsTMP.gameObject, textFromLeft, -100, 0.5f));
                yield return StartCoroutine(RunAnimation(timePointsTMP.gameObject, textFromLeft, -100, 0.5f));
                yield return StartCoroutine(RunAnimation(targetPointsTMP.gameObject, textFromLeft, -100, 0.5f));
                yield break;
            case PlayerType.Opponent:
                yield return StartCoroutine(RunAnimation(pointsTMP.gameObject, textFromRight, 220, 0.5f));
                yield return StartCoroutine(RunAnimation(pts, textFromRight, 220, 0.2f));
                yield return StartCoroutine(RunAnimation(sizePointsTMP.gameObject, textFromRight, 220, 0.5f));
                yield return StartCoroutine(RunAnimation(accuracyPointsTMP.gameObject, textFromRight, 220, 0.5f));
                yield return StartCoroutine(RunAnimation(timePointsTMP.gameObject, textFromRight, 220, 0.5f));
                yield return StartCoroutine(RunAnimation(targetPointsTMP.gameObject, textFromRight, 220, 0.5f));
                yield break;
        }
        yield break;
    }

    IEnumerator RunAnimation(GameObject go, AnimationCurve curve, float startX, float time)
    {
        RectTransform rect = go.GetComponent<RectTransform>();
        Vector3 startPoint = new Vector3(startX, rect.position.y, rect.position.z);
        curve = AnimationCurve.Linear(0, startX, time, rect.position.x);
        rect.position = startPoint;
        go.SetActive(true);
        float timer = 0f;
        while(timer<=time)
        {
            timer += Time.deltaTime;
            rect.position = new Vector3 (curve.Evaluate(timer), rect.position.y, rect.position.z);

            if (timer == time)
                yield break;
            yield return null;
        }

        yield break;
    }
}
